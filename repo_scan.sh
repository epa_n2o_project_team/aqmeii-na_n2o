#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Utility to scan status of CMAQ-build repos: NOT part of the build/run.

### TODO:
### * Find a way to point to all the repos from one host (e.g., tlrW510, which typically hosts repo=AQMEII-NA_N2O)
### * Refactor with the (should be) very similar repo_branch.sh (currently still in repo=CMAQ-build).

#---------------------------------------------------------------------- 
# constants
#---------------------------------------------------------------------- 

### message-related

THIS_FP="$0"
# THIS_DIR="$(dirname ${THIS_FP})"
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))"
THIS_FN="$(basename ${THIS_FP})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### pointers to "included" repositories

## CMAQ build/run repos host on HPCC:infinity, since they need to run on the cluster.
## These repos happen to be actual subdirs/folders of the root.
INFINITY_REPO_ROOT='/project/inf35w/roche/CMAQ-5.0.1/git/old'

## Input processing repos run on EMVL:terrae, since they need both NCL and R (which is a troublesome combination on HPCC).
## These repos happen to be symlinks from the root dir/folder.
TERRAE_REPO_ROOT="${HOME}/code/repo_scan"

### Repo commands

## this should ease refactoring repo_branch.sh
REPO_SCAN_CMD="git status | grep -ve '\.gz$\|\.log$\|\.nc$\|\.ncf$\|\.pdf$\|\.txt$\|\.zip$'"
#REPO_FIND_CMD="find ${REPO_ROOT}/ -maxdepth 1 -type d | grep -ve '/$' | grep -ve 'BLD_\|RUN_' | sort"

#---------------------------------------------------------------------- 
# payload
#---------------------------------------------------------------------- 

for REPO_ROOT in "${INFINITY_REPO_ROOT}" "${TERRAE_REPO_ROOT}" ; do
#  echo -e "REPO_ROOT='${REPO_ROOT}'"
  if [[ -r "${REPO_ROOT}" ]] ; then
    # -L -> follow symlinks: required for TERRAE_REPO_ROOT (which relies on symlinks)
    for REPO_DIR in $(find -L ${REPO_ROOT}/ -maxdepth 1 -type d | grep -ve '/$' | grep -ve 'BLD_\|RUN_' | sort) ; do
#      echo -e "REPO_DIR='${REPO_DIR}'"
      for CMD in \
	"pushd ${REPO_DIR}" \
	"${REPO_SCAN_CMD}" \
	"popd" \
      ; do
	echo -e "$ ${CMD}"
        eval "${CMD}"
      done
      echo # newline
    done
  else
    echo -e "${MESSAGE_PREFIX} skipping '$(hostname --long):${REPO_ROOT}/' (cannot read)"
    echo # newline
  fi # if [[ -r "${REPO_ROOT}" ]]
done
