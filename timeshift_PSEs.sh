#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Timeshift PSEs (point source emissions) for AQMEII-NA to spinup period (in 2007) from corresponding dates in 2008
### using `m3tshift` from IOAPI: see http://niceguy.wustl.edu/mapserver/temp/HTML/M3TSHIFT.html
### Details? see https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Running_CMAQ-5.0.1#rst-header-vs-oaqps

### Requires
### * `m3tshift`
### * `bash` with arithmetic

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### Get these out of the way before we get to work: message-related
THIS_FP="$0"
# THIS_DIR="$(dirname ${THIS_FP})"
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))"
THIS_FN="$(basename ${THIS_FP})"

### logging-related
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
# just log to screen
# DATETIME_FORMAT='%Y%m%d_%H%M'
# DATETIME_STAMP="$(date +${DATETIME_FORMAT})"
# LOG_FN="${THIS_FN}_${DATETIME_STAMP}.log"
# # log is local to this script, not build space
# export LOG_FP="${THIS_DIR}/${LOG_FN}"

### time-related

## dates:
## MMDD are invariant (same both years), but JJJ are not (leapyear(2008) != leapyear(2007))
## TODO: test both years for leap-ness, don't hardcode.
## Treat both as integers for the period of use (though MMDD really are not). TODO: use a "real" date library
PSE_START_MMDD=1222
PSE_END_MMDD=1231

## new: use PSEs for matching dates to timeshift to old/spinup period
PSE_NEW_YEAR=2008
PSE_NEW_START_JJJ=357
PSE_NEW_START_YYYYMMDD="${PSE_NEW_YEAR}${PSE_START_MMDD}"

## old: to which we're timeshifting (because we don't have "real" PSEs for this period)
PSE_OLD_YEAR=2007
PSE_OLD_START_JJJ="$(( PSE_NEW_START_JJJ - 1 ))" # because PSE_NEW_YEAR is leap year but PSE_OLD_YEAR is not
PSE_OLD_START_YYYYMMDD="${PSE_OLD_YEAR}${PSE_START_MMDD}"

### filesystem-related

## top of filesystem containing all PSEs
PSE_ROOT_DIR='/project/inf35w/roche/emis/2008cdc/AQonly/PSE' # on infinity

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

### A kludge that only works for the period of interest
### TODO: test argument!
function MMDD_to_new_JJJ {
  local MMDD="${1}"
  echo -e "$(( MMDD - PSE_START_MMDD + PSE_NEW_START_JJJ ))"
} # function MMDD_to_new_JJJ

### A kludge that depends on old and new years having particular leap-ness 
### TODO: test argument!
function MMDD_to_old_JJJ {
  local MMDD="${1}"
  echo -e "$(( $(MMDD_to_new_JJJ ${MMDD}) - 1 ))"
} # function MMDD_to_new_JJJ

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

### failfast

## on missing `m3tshift`
if [[ -z "$(which m3tshift)" ]] ; then
  echo -e "${ERROR_PREFIX} gotta have 'm3tshift' to do our work, exiting ..."
  exit 1
fi

## TODO: fail on downlevel `bash`

### iterate dates

for (( PSE_MMDD=${PSE_START_MMDD}; PSE_MMDD<=${PSE_END_MMDD}; PSE_MMDD++ )) ; do
  PSE_NEW_YYYYMMDD="${PSE_NEW_YEAR}${PSE_MMDD}"
  PSE_NEW_JJJ="$(MMDD_to_new_JJJ ${PSE_MMDD})"
  PSE_NEW_YYYYJJJ="${PSE_NEW_YEAR}${PSE_NEW_JJJ}"

  PSE_OLD_YYYYMMDD="${PSE_OLD_YEAR}${PSE_MMDD}"
  PSE_OLD_JJJ="$(MMDD_to_old_JJJ ${PSE_MMDD})"
  PSE_OLD_YYYYJJJ="${PSE_OLD_YEAR}${PSE_OLD_JJJ}"

#   # start debugging---------------------------------------------------
#   echo -e "${MESSAGE_PREFIX} PSE_NEW_YYYYMMDD='${PSE_NEW_YYYYMMDD}'"
#   echo -e "${MESSAGE_PREFIX} PSE_NEW_YYYYJJJ='${PSE_NEW_YYYYJJJ}'"
#   echo -e "${MESSAGE_PREFIX} PSE_OLD_YYYYMMDD='${PSE_OLD_YYYYMMDD}'"
#   echo -e "${MESSAGE_PREFIX} PSE_OLD_YYYYJJJ='${PSE_OLD_YYYYJJJ}'"
#   echo # newline
#   #   end debugging---------------------------------------------------

  echo -e "${MESSAGE_PREFIX} about to timeshift PSEs for '${PSE_NEW_YYYYMMDD}' to '${PSE_OLD_YYYYMMDD}'\n"

  ### iterate required PSE "types"

  for PSE_TYPE in 'ptfire' 'ptipm' ; do # TODO: iterate array
    PSE_DIR="${PSE_ROOT_DIR}/${PSE_TYPE}"
    PSE_NEW_FN="inln_mole_${PSE_TYPE}_${PSE_NEW_YYYYMMDD}_12US1_cmaq_cb05_soa_2008aa_08c.ncf"
    PSE_NEW_FP="${PSE_DIR}/${PSE_NEW_FN}"
    PSE_OLD_FN="inln_mole_${PSE_TYPE}_${PSE_OLD_YYYYMMDD}_12US1_cmaq_cb05_soa_2008aa_08c.ncf"
    PSE_OLD_FP="${PSE_DIR}/${PSE_OLD_FN}"

    ### What can we do?

    if   [[ -z "${PSE_OLD_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} PSE_OLD_FP not defined, exiting ..."
      exit 2
    elif [[ -r "${PSE_OLD_FP}" ]] ; then
      echo -e "${MESSAGE_PREFIX} skipping existing file='${PSE_OLD_FP}'"
    elif [[ -z "${PSE_NEW_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} PSE_NEW_FP not defined, exiting ..."
      exit 3
    elif [[ ! -r "${PSE_NEW_FP}" ]] ; then
      echo -e "${ERROR_PREFIX} cannot read input='${PSE_NEW_FP}', exiting ..."
      exit 4
    else # [[ -r "${PSE_NEW_FP}" && ! -r "${PSE_OLD_FP}" ]]

      ## before timeshift

      for CMD in \
        "ls -alh ${PSE_NEW_FP}" \
      ; do
        echo -e "${MESSAGE_PREFIX} ${CMD}"
        eval "${CMD}"
      done

      ## do the timeshift! (to the tune of "do the hustle" :-)
      echo -e "${MESSAGE_PREFIX} about to 'm3tshift' PSE_NEW_FP:${PSE_NEW_YYYYJJJ} to PSE_OLD_FP:${PSE_OLD_YYYYJJJ}"
      export PSE_OLD_FP
      export PSE_NEW_FP

      ## NOTE: `m3tshift` will fail to take the Julian if it's followed by a comment!
      ## unlike the other fields :-( truly annoying ...
      m3tshift << EOHD > /dev/null # all the IOAPI apps are astonishingly verbose by default
PSE_NEW_FP         # name of env var pointing to input, not its value
                   # use input starting Julian from input:SDATE
                   # use input starting time from input:STIME
${PSE_OLD_YYYYJJJ}
                   # use output starting time from input:STIME
                   # use output timestep from input:TSTEP
                   # use output duration = input:TSTEP * |input:TSTEP|
PSE_OLD_FP         # name of env var pointing to output, not its value
EOHD
      # TODO: test errorcode from m3tshift

      ## after timeshift
      if [[ ! -r "${PSE_OLD_FP}" ]] ; then
	echo -e "${ERROR_PREFIX} failed to timeshift output='${PSE_OLD_FP}', exiting ..."
        exit 5

      else

	for CMD in \
	  "ls -alh ${PSE_OLD_FP}" \
	; do
	  echo -e "${MESSAGE_PREFIX} ${CMD}"
	  eval "${CMD}"
	done
    
      fi # if [[ ! -r "${PSE_OLD_FP}" ]]
  
    fi # if   [[ -z "${PSE_OLD_FP}" ]]
    echo # newline

  done # for PSE_TYPE in 'ptfire' 'ptipm'

done # for (( PSE_MMDD=${PSE_START_MMDD}
echo # newline

exit 0 # success!
